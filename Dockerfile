FROM node:14.2.0-alpine
RUN npm rebuild
RUN mkdir /var/www
WORKDIR /var/www
ADD package.json /var/www/package.json
COPY . .
RUN npm rebuild
RUN yarn install
RUN yarn build
CMD ["yarn", "start"]