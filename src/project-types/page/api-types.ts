import RESPONSE_CODES from '@project-types/common/response-statuses';
import { ActionRequestType } from '@utils/api/types';
import { PageType } from './entityes';

export type PageEditableFields = Omit<PageType, 'id'>
export interface GetItemType extends ActionRequestType {
    params: {
        bookId?: string,
        pageId?: string
    },
    success: {
        body: {
            page: PageType
        },
        status: RESPONSE_CODES.OK
    },
    fail: {
        body: {
            e: [string]
        },
        status: RESPONSE_CODES.NOT_FOUND
    }
}

export interface PostItemType extends ActionRequestType {
  request: {
    body?: {
      page?: PageEditableFields
    }
  }
  success: {
    body: {
      page: PageType
    },
    status: RESPONSE_CODES.OK
  },
  fail: {
    body: {
      e: [string]
    },
    status: RESPONSE_CODES.NOT_FOUND
  }
}
// export type PostType = ActionContext<GetByIdType, undefined, { pages: Array<PageType>,  }

// export type PostType =  {
//     params: {
//         bookId?: number,
//     },
//     body: {
//         pages: Array<PageType>
//     } | NotFoundType,
//     status: RESPONSE_CODES.OK | RESPONSE_CODES.NOT_FOUND
// }
