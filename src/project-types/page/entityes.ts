export interface PageType {
  id: string,
  title: string,
  blocks: Array<string>,
}
