import { PageType } from '@project-types/page/entityes';

export interface BooksType {
    id: string,
    name: string,
    author: string,
    cover: string,
    numOfPages: number,
    pages: Array<PageType>,
}
