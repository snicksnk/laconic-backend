import { ActionRequestType } from '@utils/api/types';
import RESPONSE_CODES from '@project-types/common/response-statuses';
import { BooksType } from './entityes';

export type BookEditableFields = Omit<BooksType, 'id' | 'pages'>
export interface GetItemType extends ActionRequestType {
  params: {
    bookId?: string
  },
  success: {
    body: {
      book: BookEditableFields
    },
    status: RESPONSE_CODES.OK
  },
  fail: {
    body: {
      e: [string]
    },
    status: RESPONSE_CODES.NOT_FOUND
  }
}

export interface PostItemType extends ActionRequestType {
  request: {
    body?: {
      book?: BookEditableFields
      // book?: Partial<BooksType>
    }
  }
  success: {
    body: {
      book: BooksType
    },
    status: RESPONSE_CODES.OK
  },
  fail: {
    body: {
      e: [string]
    },
    status: RESPONSE_CODES.NOT_FOUND
  }
}

export interface PatchItemType extends PostItemType {
  params: {
    bookId?: string
  },
}

export interface DeleteItemType extends Omit<GetItemType, 'success'> {
  success: {
    body: {},
    status: RESPONSE_CODES.OK
  },
}

export interface GetListType {
  success: {
    body: {
      bookList: Array<BookEditableFields>
    },
    status: RESPONSE_CODES.OK
  },
  fail: {
    body: {
      e: [string]
    },
    status: RESPONSE_CODES.NOT_FOUND
  },
}
// export interface GetListType extends ContextType {
//   body: {
//     booksList: Array<BooksType> | CommonError
//   },
// }

// export interface CreateBookType extends ContextType {
//   request: {
//     body?: {
//       book: Omit<BooksType, 'id'>
//     }
//   },
//   body: {
//     book: BooksType
//   } | CommonError
// }
// export interface GetBookType extends ContextType {
//   params: {
//     id: BooksType['id'],
//   },
//   body: {
//     book: BooksType
//   } | CommonError
// }

// export interface DeleteBookType extends ContextType {
//   params: {
//     id: BooksType['id'],
//   },
//   body: {
//   } | CommonError
// }
