// eslint-disable-next-line no-shadow
enum RESPONSE_CODES {
    NOT_FOUND = 404,
    OK = 200,
    FORBIDDEN = 403,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    INTERBAL_ERROR = 500,
}

export default RESPONSE_CODES;
