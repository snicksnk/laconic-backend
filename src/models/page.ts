import { PageType } from '@project-types/page/entityes';
import {
  Document, model, Schema,
} from 'mongoose';

export interface PageModelType extends Omit<PageType, 'id'>, Document {
}

const PageSchema = new Schema<PageModelType>({
  title: { type: String },
  blocks: { type: [String] },
});

const PageModel = model<PageModelType>('Page', PageSchema, 'Page');

export default PageModel;
