import { BooksType } from '@project-types/books/entityes';
import {
  Document, model, Schema,
} from 'mongoose';
import PageModel, { PageModelType } from './page';

export interface BookModelType extends Omit<BooksType, 'id' | 'pages'>, Document {
  pages: Array<PageModelType>
}

const BookSchema = new Schema<BookModelType>({
  name: { type: String, required: true },
  author: { type: String, required: true },
  cover: { type: String, required: true },
  numOfPages: { type: Number, required: true },
  pages: [PageModel.schema],
});

const BookModel = model<BookModelType>('book', BookSchema, 'book');

export default BookModel;
