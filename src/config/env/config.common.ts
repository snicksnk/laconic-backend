const CONFIG = {
  SERVER: {
    PORT: 5000 || process.env.PORT,
    PREFIX: '/api',
  },
  SERVICES: {
    AUTH: {
      URL: 'http://185.197.14.206:30383',
    },
    QUEST_DEV_URL: 'http://quil.ottereducation.co.uk:8888/api/v1',
  },
  token: 'secret-jwt-token',
  DATABASE: {
    HOST: process.env.DB_HOST || 'localhost',
    DATABASE_NAME: process.env.DB_NAME || 'otter',
    PORT: process.env.DB_PORT || 27019,
    OPTIONS: {
      autoIndex: false,
      connectTimeoutMS: 1000,
      user: process.env.DB_USER,
      pass: process.env.DB_PASSWORD,
      auth: {
        authdb: process.env.DB_AUTHSOURCE,
      },
    },
  },
  STRIPE: {
    SK:
      process.env.STRIPE_SK
      || 'sk_test_51HNfFpCS0ftqW8lkA4vcPLiJXSXj4CUlvoaM3BswWpskMK7BTZVgDkIbPRJ9nXmjhVHsIw4JEZH4BJJhszKKnhWf00Ot2uqUTa',
    WEBHOOK_KEY:
      process.env.WEBHOOK_KEY || 'whsec_j2o3d3jvtaCpUN4aNFM1IcuVJEazhAmd',
  },
  EXAM: {
    MAX_QUESTIONS: 20,
    DURATION: 40,
    EXTENTION: 20,
    MAX_LEVEL: 6,
  },
  MAILER: {
    MANDRILL_KEY: process.env.MANDRILL_KEY || 'd57BpJl-1I04VX2k6LD1Xw',
    MAILCHIMP_KEY:
      process.env.MAILCHIMP_KEY || 'bf8fe8a2314f60710f89bb400633945d-us2',
    MAILCHIMP_SERVER: process.env.MAILCHIMP_SERVER || 'us2',
    DEFAULT_MAIL_LIST: {
      CONTACT_INFO: {
        company: 'Otter inc.',
        address1: 'ul. Pushkina 999-666',
        city: 'Uljanovsk',
        state: 'Uljanovskaya Oblast',
        zip: '432001',
        country: 'Russia',
      },
      CAMPAIGN_DEFAULTS: {
        from_name: 'Otter Admin',
        from_email: 'CONTACT@WONDERCREATURES.COM',
        subject: 'Otter Notification',
        language: 'english',
      },
    },
  },
  ADMIN: {
    MAX_DELETE_COUNT: 4,
    ACL: {
      ADMIN: 'ADMIN',
      SUPER: 'SUPER_ADMIN',
      CEO: 'CEO',
      USER: 'USER',
    },
  },
};

export default CONFIG;
