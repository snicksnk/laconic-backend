import merge from 'lodash/merge';
import COMMON_CONFIG from './env/config.common';

const env = process.env.NODE_ENV || 'development';
/* eslint import/no-dynamic-require: 0 */
const config = require(`./env/config.${env}`).default;

export default merge(COMMON_CONFIG, config);
