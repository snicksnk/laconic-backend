import { action } from '@utils/api/action';
import presenter from '@utils/api/presenter';
import { applyRestRouter, createRestRouter } from '@utils/api/rest-router';
import Router from '@koa/router';
import { getBook } from '@modules/books/controller';
import * as controller from './controller';
import { pagePresentor } from './presenter';

// const router = new Router<{}, IDatabaseMiddleware>();

export function pagesConfigureRouter(appRouter: Router) {
  const router = createRestRouter({ prefix: '/books/:bookId' });

  router.register('/pages/:pageId', ['GET'], [
    action(getBook),
    action(controller.getPage),
    presenter(pagePresentor),
  ]);

  router.register('/pages/', ['POST'], [
    action(getBook),
    action(controller.postPage),
    presenter(pagePresentor),
  ]);

  applyRestRouter(appRouter, router);
}

// router.get('/', (ctx) => { ctx.body = 123 } )
// const GetChain: [
//   typeof action<GetItemType, { pageModle: PageType} >
// ] = [
//   action(controller.getPages),
//   presenter(pagePresentor),
// ]

// type UsedDocument = {
//   id: string,
//   name: string
// };
// type BookDocument = {};
// type PageDcoument = {};

// type middlwareChainType = [
//   () => UsedDocument,
//   (user: UsedDocument) => BookDocument,
//   (bookDocument: BookDocument) => PageDcoument,
//   (pageDocument: PageDcoument) => void
// ];

// const middlwareChain: middlwareChainType = [
//   () => {
//     const user: UsedDocument = {
//       id: 'as',
//       name: 'assasa'
//     };
//     return user;
//   },
//   (user) => {
//     const bookDocument: BookDocument = {

//     }
//     return bookDocument;
//   },
//   (bookDocument) => {
//     const pageDocument: PageDcoument = {
//     }
//     return pageDocument;
//   },
//   (pageDocument) => {
//   }
// ]
