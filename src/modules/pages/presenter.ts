import { presentorCallbackType } from '@utils/api/types';
import { defaultModelPresentor } from '@utils/models/defaultModelPresentor';
import { PageModelType } from '@models/page';
import RESPONSE_CODES from '@project-types/common/response-statuses';
import { GetItemType } from '@project-types/page/api-types';

export const pagePresentor: presentorCallbackType<GetItemType, { pageModel: PageModelType }> = (state) => {
  const { pageModel } = state;
  // ctx.status = RESPONSE_CODES.OK;

  // TODO без отдельной переменной валидации не работает
  const body: GetItemType['success'] = {
    status: RESPONSE_CODES.OK,
    body: {
      page: defaultModelPresentor<PageModelType>(pageModel),
    },
  };

  return body;
};
