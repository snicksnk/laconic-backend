import { cleanAction } from '@utils/api/types';
import { PageModelType } from '@models/page';
import { GetItemType, PostItemType } from '@project-types/page/api-types';
import { stateWithBookModelType } from '@modules/books/types';
import { getPageFromDb, createPageToBook } from './service';

// export const getPages: actionType<GetItemType> = async (ctx, next) => {
//   const { bookId } = ctx.params;

//   try {
//     ctx.body = {
//       pages: [
//         { id: '1', title: 'dsads' },
//       ],
//     };
//     // const tree = await getTree();
//   } catch (err) {
//     ctx.body = { e: ['Book is not found'] };
//     ctx.status = 400;
//   }
//   await next();
// };

export const getPage: cleanAction<GetItemType & { state: stateWithBookModelType }, { pageModel: PageModelType }> = async (ctx) => {
  const pageId = ctx.params?.pageId;
  const bookModel = ctx.state?.bookModel;

  if (!bookModel) {
    throw new Error('bookModel is not provided');
  }

  if (!pageId) {
    throw new Error('Params error');
  }

  const page = await getPageFromDb(bookModel, pageId);

  if (!page) {
    throw new Error('Page is not found');
  }

  return {
    pageModel: page,
  };
};

export const postPage: cleanAction<PostItemType & { state: stateWithBookModelType }, { pageModel: PageModelType }> = async (ctx) => {
  const pageParams = ctx.request?.body?.page;
  const bookModel = ctx.state?.bookModel;

  if (!bookModel) {
    throw new Error('bookModel is not provided');
  }

  if (!pageParams) {
    // throwError(RESPONSE_CODES.BAD_REQUEST, { e: 'Bad pram' })
    throw new Error('Wrong params');
  }

  const page = await createPageToBook(bookModel, pageParams);

  if (page === null) {
    throw new Error('Create book error');
  }

  return {
    pageModel: page,
  };
};
