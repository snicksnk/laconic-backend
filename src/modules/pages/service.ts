import BookModel, { BookModelType } from '@models/book';
import { PageType } from '@project-types/page/entityes';
import PageModel, { PageModelType } from '@models/page';
import { PageEditableFields } from '@project-types/page/api-types';

export async function getPageFromDb(bookModel: BookModelType, pageId: PageType['id']): Promise<PageModelType | null> {
  // const page = await bookMode

  const book = await BookModel.findOne(
    { _id: bookModel.id, 'pages._id': pageId },
    { 'pages.$': 1 },
  );

  const page = book?.pages[0];
  if (page) {
    return new PageModel(page);
  }

  return null;
}

export async function createPageToBook(bookModel: BookModelType, pageData: PageEditableFields): Promise<PageModelType | null> {
  const page = new PageModel(pageData);
  bookModel.pages.push(page);
  await bookModel.save();
  return bookModel.pages[bookModel.pages.length - 1];
}
