import supertest from 'supertest';
import { cleanDb /* , authUser */ } from '@utils/tests/utils';
import app from '@app/server';
import BookModel, { BookModelType } from '@app/models/book';

const request = supertest.agent(app.callback());

beforeAll(() => {
  // runs before all tests in this block
  cleanDb();
});

const urlPrefix = '/api/books';

afterAll(() => {
  cleanDb();
});

describe('Pages', () => {
  const page1 = {
    title: 'Bitcoin',
    blocks: [
      'bloc1',
      'bloclk2',
    ],
  };
  const page2 = {
    title: 'Etherium',
    blocks: [
      'bloc1',
      'bloclk2',
    ],
  };
  const bookData = {
    name: 'new book',
    author: 'Pushkin',
    cover: '1.jpg',
    numOfPages: 200,
    pages: [page1],
  };
  let bookId:string;
  let bookModel: BookModelType;

  beforeEach(async () => {
    cleanDb();
    bookModel = new BookModel(bookData);
    await bookModel.save();
    bookId = bookModel.id;
  });
  describe('Pages crud', () => {
    it('Get page from db', async () => {
      const page1Model = bookModel.pages[0];

      const response = await request
        .get(`${urlPrefix}/${bookId}/pages/${page1Model.id}`);

      const { body, status } = response;

      expect(status).toBe(200);
      expect(body).toMatchObject({
        page: {
          ...page1,
          id: page1Model.id,
        },
      });
    });

    it('Create page in db', async () => {
      const response = await request
        .post(`${urlPrefix}/${bookId}/pages/`)
        .send({ page: page2 });

      const { body, status } = response;
      expect(status).toBe(200);

      const bookModelUpdated = await BookModel.findById(bookId);
      if (!bookModelUpdated) {
        throw Error('Book model is not found');
      }
      const page2Model = bookModelUpdated.pages[1];
      expect(body).toMatchObject({
        page: {
          ...page2,
          id: page2Model.id,
        },
      });
    });
  });
});
