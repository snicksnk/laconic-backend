import Router from '@koa/router';
import { createRestRouter, applyRestRouter } from '@utils/api/rest-router';
import { action } from '@utils/api/action';
import presenter from '@utils/api/presenter';
import * as controller from './controller';
import { bookListPresentor, bookPresentor, emptyPresentor } from './presenter';

// const router = new Router<{}, IDatabaseMiddleware>();

export function booksConfigureRouter(appRouter: Router) {
  const router = createRestRouter({ prefix: '/books' });

  router.register('/:bookId', ['GET'], [
    action(controller.getBook),
    presenter(bookPresentor),
  ]);

  router.register('/', ['POST'], [
    action(controller.postBook),
    presenter(bookPresentor),
  ]);

  router.register('/:bookId', ['PATCH'], [
    action(controller.getBook),
    action(controller.patchBook),
    presenter(bookPresentor),
  ]);

  router.register('/:bookId', ['DELETE'], [
    action(controller.getBook),
    action(controller.deleteBook),
    presenter(emptyPresentor),
  ]);

  router.register('/', ['GET'], [
    action(controller.getBooksList),
    presenter(bookListPresentor),
  ]);
  applyRestRouter(appRouter, router);
}
