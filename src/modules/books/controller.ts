import {
  DeleteItemType,
  GetItemType, GetListType, PatchItemType, PostItemType,
} from '@project-types/books/api-types';

import {
  getBookFromDb,
  createBookToDb,
  updateBookFromDb,
  deleteBookFromDb,
  getBooksListFromDb,
} from '@modules/books/service';
import { cleanAction } from '@utils/api/types';
import RESPONSE_CODES from '@project-types/common/response-statuses';
import { stateWithBookModelType, stateWithBookModelListType } from './types';

export const postBook: cleanAction<PostItemType, stateWithBookModelType> = async (ctx) => {
  const bookParams = ctx.request?.body?.book;

  if (!bookParams) {
    // throwError(RESPONSE_CODES.BAD_REQUEST, { e: 'Bad pram' })
    throw new Error('Wrong params');
  }

  const book = await createBookToDb(bookParams);

  if (book === null) {
    throw new Error('Create book error');
  }

  return {
    bookModel: book,
  };
};

export const getBook: cleanAction<GetItemType, stateWithBookModelType> = async (ctx) => {
  const { bookId } = ctx.params;

  if (!bookId) {
    // throwError(RESPONSE_CODES.BAD_REQUEST, { e: 'Bad pram' })
    throw new Error('wrong params');
  }

  const book = await getBookFromDb(bookId);

  if (book === null) {
    throw new Error(`${RESPONSE_CODES.NOT_FOUND}`);
  }

  return {
    bookModel: book,
  };
};

export const patchBook: cleanAction<PatchItemType, stateWithBookModelType> = async (ctx) => {
  const { bookId } = ctx.params;
  const bookParams = ctx.request?.body?.book;

  if (!bookId || !bookParams) {
    // throwError(RESPONSE_CODES.BAD_REQUEST, { e: 'Bad pram' })
    throw new Error('wrong params');
  }

  const book = await updateBookFromDb(bookId, bookParams);

  if (book === null) {
    throw new Error(`${RESPONSE_CODES.NOT_FOUND}`);
  }

  return {
    bookModel: book,
  };
};

export const deleteBook: cleanAction<DeleteItemType, {}> = async (ctx) => {
  const { bookId } = ctx.params;
  if (!bookId) {
    // throwError(RESPONSE_CODES.BAD_REQUEST, { e: 'Bad pram' })
    throw new Error('wrong params');
  }
  await deleteBookFromDb(bookId);
  return {};
};

export const getBooksList: cleanAction<GetListType, stateWithBookModelListType> = async () => {
  const books = await getBooksListFromDb();
  return {
    bookModelList: books,
  };
};

// export
// const createBookAction: apiActionType<CreateBookParams> = async (params, throwError) => {
//   const { bookParams } = params;
//   if (!bookParams) {
//     throwError(RESPONSE_CODES.BAD_REQUEST, 'Fields error');
//   }
//   const bookModel = await createBookToDb(bookParams);
//   return { bookModel };
// };

// export const deleteBook: Router.IMiddleware = async (ctx: DeleteBookType, next) => {
//   const { id: bookId } = ctx.params;
//   const book = await deleteBookFromDb(bookId);

//   if (!book) {
//     ctx.status = RESPONSE_CODES.NOT_FOUND;
//     return;
//   }

//   await next();
// };
