import { BooksType } from '@project-types/books/entityes';
import BookModel, { BookModelType } from '@models/book';
import { BookEditableFields } from '@project-types/books/api-types';

export async function createBookToDb(bookData: BookEditableFields): Promise<BookModelType> {
  const book = new BookModel(bookData);
  await book.save();
  return book;
}

export async function getBooksListFromDb(): Promise<Array<BookModelType>> {
  const books = await BookModel.find({}, { 'pages.blocks': 0 });
  return books;
}

export async function getBookFromDb(bookId: BooksType['id']): Promise<BookModelType | null> {
  const book = await BookModel.findOne({ _id: bookId }, { 'pages.blocks': 0 });
  return book;
}

export async function updateBookFromDb(bookId: BooksType['id'], bookData: BookEditableFields): Promise<BookModelType | null> {
  const book = await BookModel.findByIdAndUpdate(bookId, bookData, {
    new: true,
  });
  return book;
}

// не работает
export async function deleteBookFromDb(bookId: BooksType['id']): Promise<void> {
  await BookModel.findByIdAndDelete(bookId);
}
