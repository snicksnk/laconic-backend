import { presentorCallbackType } from '@utils/api/types';
import { defaultModelPresentor } from '@utils/models/defaultModelPresentor';
import { DeleteItemType, GetItemType, GetListType } from '@project-types/books/api-types';
import RESPONSE_CODES from '@project-types/common/response-statuses';
import { BookModelType } from 'models/book';
import { PageModelType } from '@app/models/page';

const bookModelPresenter = (bookModel: BookModelType) => (
  {
    ...defaultModelPresentor<BookModelType>(bookModel),
    pages: bookModel.pages.map((page: PageModelType) => defaultModelPresentor(page)),
  }
);

export const bookPresentor: presentorCallbackType<GetItemType, { bookModel: BookModelType }> = (state) => {
  const { bookModel } = state;
  // ctx.status = RESPONSE_CODES.OK;

  // TODO без отдельной переменной валидации не работает
  const body: GetItemType['success'] = {
    status: RESPONSE_CODES.OK,
    body: {
      book: bookModelPresenter(bookModel),
    },
  };

  return body;
};

export const emptyPresentor: presentorCallbackType<DeleteItemType, { bookModel: BookModelType }> = () => {
  const body: DeleteItemType['success'] = {
    status: RESPONSE_CODES.OK,
    body: {
      message: 'Action successfull',
    },
  };

  return body;
};

export const bookListPresentor: presentorCallbackType<GetListType, { bookModelList: Array<BookModelType> }> = (state) => {
  const { bookModelList } = state;
  // ctx.status = RESPONSE_CODES.OK;

  // TODO без отдельной переменной валидации не работает
  const body: GetListType['success'] = {
    status: RESPONSE_CODES.OK,
    body: {
      bookList: bookModelList.map((book: BookModelType) => (bookModelPresenter(book))),
    },
  };

  return body;
};
