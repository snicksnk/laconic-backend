import supertest from 'supertest';
import { cleanDb /* , authUser */ } from '@utils/tests/utils';
import app from '@app/server';
import BookModel, { BookModelType } from '@app/models/book';
import { BookEditableFields } from '@project-types/books/api-types';
import { getBookFromDb } from '../service';

const request = supertest.agent(app.callback());

beforeAll(() => {
  // runs before all tests in this block
  cleanDb();
});

const urlPrefix = '/api/books';

afterAll(() => {
  cleanDb();
});

describe('Books', () => {
  const bookData: BookEditableFields = {
    name: 'new book',
    author: 'Pushkin',
    cover: '1.jpg',
    numOfPages: 200,
  };

  const bookData2: BookEditableFields = {
    ...bookData,
    name: 'old book',
  };

  beforeEach(async () => {
    // runs before all tests in this block
    cleanDb();
  });
  describe('Books crud', () => {
    it('Get book from db', async () => {
      const bookModel: BookModelType = new BookModel(bookData);
      await bookModel.save();
      const response = await request
        .get(`${urlPrefix}/${bookModel.id}`);

      const { body, status } = response;
      expect(status).toBe(200);
      expect(body).toMatchObject({
        book: {
          ...bookData,
          id: bookModel.id,
        },
      });
    });

    it('Create book', async () => {
      // const bookModel: BookModelType = new BookModel(booksData);
      // await bookModel.save();
      // const { body: presentBody } = bookPresentor({ bookModel });

      const response = await request
        .post(`${urlPrefix}`)
        .send({ book: bookData });

      const { body, status } = response;
      expect(status).toBe(200);

      const bookModel = await BookModel.findById(body.book.id);
      if (!bookModel) {
        throw Error('Book model is not found');
      }

      expect(body).toMatchObject({
        book: {
          ...bookData,
          id: bookModel.id,
        },
      });
    });

    it('Update book', async () => {
      const updateBookData: Partial<BookEditableFields> = {
        name: 'new book2',
      };

      const bookModel: BookModelType = new BookModel(bookData);
      await bookModel.save();

      const response = await request
        .patch(`${urlPrefix}/${bookModel.id}`)
        .send({ book: updateBookData });

      const { body, status } = response;

      expect(status).toBe(200);
      expect(body).toMatchObject({
        book: { ...bookData, ...updateBookData },
      });
    });

    it('Delete book from db', async () => {
      const bookModel: BookModelType = new BookModel(bookData);
      await bookModel.save();
      // const { body: presentBody } = bookPresentor({ bookModel });

      const response = await request
        .delete(`${urlPrefix}/${bookModel.id}`);

      const { status } = response;

      expect(status).toBe(200);
      expect(await getBookFromDb(bookModel.id)).toBeNull();
    });

    it('Get book list from db', async () => {
      const bookModel1: BookModelType = new BookModel(bookData);
      await bookModel1.save();

      const bookModel2: BookModelType = new BookModel(bookData2);
      await bookModel2.save();

      const response = await request
        .get(`${urlPrefix}/`);

      const { body, status } = response;
      expect(status).toBe(200);
      expect(body).toMatchObject({
        bookList:
        [
          {
            ...bookData,
            id: bookModel1.id,
          },
          {
            ...bookData2,
            id: bookModel2.id,
          },
        ],
      });
    });
  });
});
