import { BookModelType } from '@models/book';

export type stateWithBookModelType = { bookModel: BookModelType };
export type stateWithBookModelListType = { bookModelList: Array<BookModelType> };
