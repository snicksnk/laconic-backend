/* eslint-disable */
import mongoose from 'mongoose';


export function cleanDb() {
  for (const collection in mongoose.connection.collections) {
    if (mongoose.connection.collections.hasOwnProperty(collection)) {
      mongoose.connection.collections[collection].remove({});
    }
  }
}

export function closeConnection(connection: any) {
  return Promise.all([connection.disconnect(), mongoose.connection.close()]);
}

interface authUserType {
  url: string,
  user: any,
  request: any,
}

export async function authUser({ url = '/api/auth', user, request } : authUserType) {
  const response = await request
    .post(url)
    .send(user);

  const { token, user: userData } = response.body;
  return { token, user: userData };
}
