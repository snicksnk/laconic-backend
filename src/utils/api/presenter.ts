import { RouterContext } from '@koa/router';
import {
  actionType, ActionRequestType, presentorCallbackType,
} from './types';

const presenter = <CTX extends ActionRequestType, STATE_VALUE>
  (presentorCallback: presentorCallbackType<CTX, STATE_VALUE>): actionType<CTX> => async (ctx, next) => {
    // fix this any
    const { body } = presentorCallback((ctx as RouterContext).state);
    (ctx as RouterContext).body = body;
    await next();
  };

export default presenter;
