import { RouterContext } from '@koa/router';
import {
  ActionRequestType, cleanAction, actionType, throwErrorType,
} from './types';

// export type actionssType = getPages<CT> = Middleware<RouterContext, <CT>>>

// вынести

export type actionWrapperType = <CTX extends ActionRequestType, RETURN_STATE>
  (actionHandler: cleanAction<CTX, RETURN_STATE>) => actionType<CTX>;

export const action: actionWrapperType = <CTX extends ActionRequestType, RETURN_STATE>
  (actionHandler: cleanAction<CTX, RETURN_STATE>) => async (ctx, next) => {
    const throwError: throwErrorType = (status, e) => {
      (ctx as RouterContext).throw(status, e);
    };

    const newStateData = await actionHandler(ctx, throwError);
    Object.assign((ctx as RouterContext).state, newStateData);
    await next();
  };

// export const action2 = <CTX extends ActionRequestType, RETURN_STATE>
//   (actionHandler: cleanAction<CTX, RETURN_STATE>): actionType<CTX> => async (ctx, next) => {
//     const newStateData = await actionHandler(ctx);

//     console.log('ctrl', newStateData);
//     Object.assign((ctx as RouterContext).state, newStateData);
//     await next();
//   };

// type GetterType<CTX, ActionDataType> = (ctx: CTX) => ActionDataType;

// export type apiActionType<CTX, RETURN_STATE> =
// (ctx: CTX, throwError?: RouterContext['throw']) => Promise<RETURN_STATE>;

// export const consumeCtx2 = <CTX extends ActionRequestType, ACTION_PARAMS, RETURN_STATE>
//    (getter: GetterType<actionContext<CTX>, ACTION_PARAMS>,
// action: apiActionType<ACTION_PARAMS, RETURN_STATE>): actionType<CTX> =>
//         async (ctx, next) => {
//         const stateData = getter(ctx);
//         const newStateData = await action(stateData, (ctx as RouterContext).throw);
//         // (ctx as RouterContext).state = { ...(ctx as RouterContext).state, ...newStateData };
//         Object.assign((ctx as RouterContext).state, newStateData)
//         await next();
// }
