import { Middleware, RouterContext } from '@koa/router';
import RESPONSE_CODES from '@project-types/common/response-statuses';

import { Next } from 'koa';

export interface ActionRequestType {
    request?: {
        body?: object,
    }
    params?: object,
    success: {
        body: {},
        status: {}
    },
    fail: {
        body: {
            e: [string]
        },
        status?: number
    },
    // TODO remove from here
    state?: object
}

interface ContextWithStateStateType extends ActionRequestType {
    state?: object
}

export type actionContext<REQUEST_TYPE extends ContextWithStateStateType> = {
    request?: REQUEST_TYPE['request'],
    params: REQUEST_TYPE['params'],
    state?: REQUEST_TYPE['state'],
} & (REQUEST_TYPE['success'] | REQUEST_TYPE['fail']);

export type defaultactionType<CTX> = (ctx: CTX, next: Next) => any & Middleware<RouterContext, CTX>;

export type actionType<ActionCtx extends ActionRequestType> = defaultactionType<actionContext<ActionCtx>>

// TODO
type actionResultState<RETURN_STATE> = Promise<RETURN_STATE>;

export type throwErrorType = (status: RESPONSE_CODES, e: object) => void

export type cleanAction<CTX extends ActionRequestType, RETURN_STATE> =
    (ctx: Readonly<actionContext<CTX>>, throwError?: throwErrorType) => actionResultState<RETURN_STATE>;

export type presentorCallbackType<CTX extends ActionRequestType, STATE_VALUE> =
    (state: STATE_VALUE) => Required<CTX['success'] | CTX['fail']>;
