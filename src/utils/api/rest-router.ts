import Router, { RouterOptions } from '@koa/router';

interface RestRouter extends Router {

}

export function createRest() {

}

export function createRestRouter(params: RouterOptions): RestRouter {
  const router = new Router(params);

  return router;
}

export function applyRestRouter(appRouter: Router, restRouter: RestRouter) {
  appRouter.use(
    restRouter.routes(),
    restRouter.allowedMethods(),
  );
}
