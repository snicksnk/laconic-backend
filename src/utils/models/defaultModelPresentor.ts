import { Document } from 'mongoose';

export function defaultModelPresentor<T extends Document>(model: T) {
  const modelRows = model.toObject();
  const { _id, __v, ...rows } = modelRows;

  return {
    id: `${_id}`,
    ...rows,
  };
}
