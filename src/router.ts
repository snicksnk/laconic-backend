import Router from '@koa/router';

import { pagesConfigureRouter } from '@modules/pages/router';
import { booksConfigureRouter } from '@modules/books/router';
import CONFIG from './config/index';

const router = new Router({
  prefix: CONFIG.SERVER.PREFIX,
});

booksConfigureRouter(router);
pagesConfigureRouter(router);

export default router;
