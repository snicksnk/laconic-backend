import Koa from 'koa';

import logger from 'koa-logger';
import json from 'koa-json';
import bodyparser from 'koa-bodyparser';
import cors from '@koa/cors';
import mongoose from 'mongoose';
import CONFIG from './config';

import router from './router';

// import setUpPassport from "./middleware/passport";

mongoose.Promise = global.Promise;
mongoose.connect(
  `mongodb://${CONFIG.DATABASE.HOST}:${CONFIG.DATABASE.PORT}/${CONFIG.DATABASE.DATABASE_NAME}`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  },
);
console.log('======================================================');
console.log(
  `mongodb://${CONFIG.DATABASE.HOST}:${CONFIG.DATABASE.PORT}/${CONFIG.DATABASE.DATABASE_NAME}`,
);
const app = new Koa();

app.use(json());
app.use(logger());
app.use(bodyparser());
app.use(cors());

// const passport = setUpPassport(CONFIG);

// app.use(passport.initialize());

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = err.message;
    ctx.app.emit('error', err, ctx);
  }
});

app.on('error', (err, ctx) => {
  console.error(err, '======🤬 Context 🤬=======: \n', ctx);
  /* centralized error handling:
   *   console.log error
   *   write error to log file
   *   save error and request information to database if ctx.request match condition
   *   ...
  */
});

app.use(router.routes()).use(router.allowedMethods());

if (process.env.NODE_ENV !== 'test') {
  app.listen(CONFIG.SERVER.PORT, () => {
    console.log(`Koa started at ${CONFIG.SERVER.PORT}`);
  });
}

export default app;
